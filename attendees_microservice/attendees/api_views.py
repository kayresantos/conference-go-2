from django.http import JsonResponse
from .models import Attendee, ConferenceVO, AccountVO
# from events.models import Conference
from common.json import ModelEncoder
# from events.api_views import ConferenceListEncoder
from django.views.decorators.http import require_http_methods
import json


class ConferenceVODetailEncounter(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
    ]

    def get_extra_data(self, o):
        count = len(AccountVO.objects.filter(email=o.email))
        if count > 0:
            return {"has_account": True}
        else:
            return {"has_account": False}


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncounter(),
    }

    def get_extra_data(self, o):
        count = len(AccountVO.objects.filter(email=o.email))
        if count > 0:
            return {"has_account": True}
        else:
            return {"has_account": False}

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            AttendeeListEncoder,
            safe=False
            )
    else:
        content = json.loads(request.body)

        try:
            conference = ConferenceVO.objects.get(id=conference_vo_id)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        AttendeeListEncoder,
        safe=False
    )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            AttendeeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "attendee" in content:
                attendee = Attendee.objects.get(id=content["attendee"])
                content["attendee"] = attendee
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Attendee does not exist"},
                status=400
            )
    Attendee.objects.filter(id=id).update(**content)

    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        attendee,
        AttendeeDetailEncoder,
        safe=False
    )
